/*!
 * © 2019 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { freeze } from './commit'
import { TrackPluginState } from './plugin'

export enum TRACK_PLUGIN_ACTIONS {
  FREEZE = 'FREEZE',
  FOCUS = 'FOCUS',
  REPLACE = 'REPLACE',
}

export default (
  state: TrackPluginState,
  /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
  action?: { type: string; [key: string]: any }
): TrackPluginState => {
  if (!action) {
    return state
  }

  switch (action.type) {
    case TRACK_PLUGIN_ACTIONS.FREEZE: {
      return {
        ...state,
        commit: freeze(state.commit),
      }
    }
    case TRACK_PLUGIN_ACTIONS.FOCUS: {
      return {
        ...state,
        focusedCommit: action.commit,
      }
    }
    case TRACK_PLUGIN_ACTIONS.REPLACE: {
      return {
        ...state,
        commit: action.commit,
      }
    }
    default: {
      return state
    }
  }
}
