/*!
 * © 2019 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { EditorState, Transaction } from 'prosemirror-state'

import { TRACK_PLUGIN_ACTIONS as c } from './actions'
import { getTrackPluginState, trackPluginKey } from './plugin'

export type Command = (
  state: EditorState,
  dispatch: (tr: Transaction) => void
) => boolean

export const focusCommit = (commit: string): Command => (state, dispatch) => {
  if (dispatch) {
    dispatch(state.tr.setMeta(trackPluginKey, { type: c.FOCUS, commit }))
  }
  return true
}

export const freezeCommit = (): Command => (state, dispatch) => {
  const { commit } = getTrackPluginState(state)
  if (!commit.steps.length) {
    return false
  }

  if (dispatch) {
    dispatch(state.tr.setMeta(trackPluginKey, { type: c.FREEZE }))
  }

  return true
}
