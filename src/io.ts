/*!
 * © 2019 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Schema } from 'prosemirror-model'
import { Step } from 'prosemirror-transform'

import { Span } from './blame'
import { Commit } from './commit'

interface JsonableCommit {
  steps: Array<{ [key: string]: unknown }>
  prev: JsonableCommit | null
  id: string
  blame: Span[]
}

const commitToJsonable = (commit: Commit): JsonableCommit => {
  return {
    ...commit,
    steps: commit.steps.map((step) => step.toJSON()),
    prev: commit.prev && commitToJsonable(commit.prev),
  }
}

export const commitToJSON = (commit: Commit): string => {
  return JSON.stringify(commitToJsonable(commit))
}

export const commitFromJSON = (
  json: string | Commit,
  schema: Schema
): Commit => {
  if (typeof json === 'string') {
    return commitFromJSON(JSON.parse(json), schema)
  }

  return {
    id: json.id,
    blame: json.blame,
    prev: json.prev ? commitFromJSON(json.prev, schema) : null,
    steps: json.steps.map((step) => Step.fromJSON(schema, step)),
  }
}
