/*!
 * © 2019 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { schema } from 'prosemirror-schema-basic'
import { Step } from 'prosemirror-transform'

import {
  applyTransform,
  Commit,
  freeze,
  initialCommit,
  smoosh,
} from '../commit'
import * as io from '../io'
import { applyTr, initialState, typeSomething } from './helpers'

test('stringify a commit and get it back', () => {
  let state = initialState()
  let commit = initialCommit()

  // set up a commit with steps and with a step in the prev
  const { tr, state: _state } = typeSomething(state, [[60, 'A']])
  state = _state
  commit = applyTransform(commit, tr)
  commit = freeze(commit)

  const { tr: tr2, state: __state } = typeSomething(state, [[53, 'B']])
  state = __state
  commit = applyTransform(commit, tr2)

  const json = io.commitToJSON(commit)
  expect(() => JSON.parse(json)).not.toThrow()

  const reconstructedCommit: Commit = io.commitFromJSON(json, schema)

  const steps = smoosh<Step>(commit, (c) => c.steps)
  const reconstructedSteps = smoosh<Step>(reconstructedCommit, (c) => c.steps)

  const nextState = applyTr(initialState(), steps)
  const reconstructedState = applyTr(initialState(), reconstructedSteps)

  expect(nextState).toEqual(reconstructedState)
})
