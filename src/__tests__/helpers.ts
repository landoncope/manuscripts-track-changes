/*!
 * © 2019 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Fragment, Slice } from 'prosemirror-model'
import { schema } from 'prosemirror-schema-basic'
import { EditorState } from 'prosemirror-state'
import { Step } from 'prosemirror-transform'

import { Span } from '../blame'

export const newDocument = () => {
  return schema.nodes.doc.create({}, [
    schema.nodes.paragraph.create({}, [
      schema.text(
        'This is the initial state of the document before any edits have been made.'
      ),
    ]),
  ])
}

export const initialState = () => {
  return EditorState.create({
    doc: newDocument(),
    schema,
  })
}

export const typeSomething = (
  state: EditorState,
  additions: Array<[pos: number, text: string]>
) => {
  const { tr } = initialState()
  additions.forEach(([pos, text]) => {
    tr.replace(pos, pos, new Slice(Fragment.from(schema.text(text)), 0, 0))
  })
  state.apply(tr)
  return { state, tr }
}

export const applyTr = (initialState: EditorState, steps: Step[]) => {
  const { tr } = initialState
  steps.forEach((step) => {
    tr.step(step)
  })
  return initialState.apply(tr)
}

export const blameRemoveUUIDS = (blame: Span[]) =>
  blame
    .map((span) => {
      if (!span.commit) {
        return null
      }
      return { from: span.from, to: span.to }
    })
    .filter(Boolean) as Span[]
