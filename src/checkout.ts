/*!
 * © 2019 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Node as ProsemirrorNode } from 'prosemirror-model'
import { EditorState } from 'prosemirror-state'
import { Step } from 'prosemirror-transform'

import { TRACK_PLUGIN_ACTIONS } from './actions'
import { Commit, freeze, smoosh } from './commit'
import { trackPluginKey } from './plugin'

export const checkout = (
  ancestorDocument: ProsemirrorNode,
  currentState: EditorState,
  commit: Commit
): EditorState => {
  // if the passed commit contains steps, then freeze it before allowing
  // more changes
  if (commit.steps.length) {
    commit = freeze(commit)
  }

  const temporaryState = EditorState.create({
    doc: ancestorDocument,
    schema: currentState.schema,
    plugins: currentState.plugins,
  })

  const { tr } = temporaryState

  tr.setMeta(trackPluginKey, {
    type: TRACK_PLUGIN_ACTIONS.REPLACE,
    commit,
  })
  tr.setMeta('addToHistory', false)

  const allSteps = smoosh<Step>(commit, (c) => c.steps)
  // replay all the steps
  allSteps.forEach((step) => tr.maybeStep(step))

  // remap the selection

  const nextState = temporaryState.apply(tr)

  return nextState
}
